package com.senati.appseminario.model;

import java.util.Date;

public class Asistente {
    private int idAsis;
    private String nombreAsis;
    private String apellidosAsis;
    private Date fecNacAsis;
    private String dniAsis;
    private String sexoAsis;
    private String emailAsis;
    private String telefonoAsis;


    //Constructor

    public Asistente() {
    }
    public Asistente(int idAsis, String nombreAsis, String apellidosAsis, Date fecNacAsis, String dniAsis, String sexoAsis, String emailAsis, String telefonoAsis) {
        this.idAsis = idAsis;
        this.nombreAsis = nombreAsis;
        this.apellidosAsis = apellidosAsis;
        this.fecNacAsis = fecNacAsis;
        this.dniAsis = dniAsis;
        this.sexoAsis = sexoAsis;
        this.emailAsis = emailAsis;
        this.telefonoAsis = telefonoAsis;
    }


    //Getter and Setter

    public int getIdAsis() {
        return idAsis;
    }

    public void setIdAsis(int idAsis) {
        this.idAsis = idAsis;
    }

    public String getNombreAsis() {
        return nombreAsis;
    }

    public void setNombreAsis(String nombreAsis) {
        this.nombreAsis = nombreAsis;
    }

    public String getApellidosAsis() {
        return apellidosAsis;
    }

    public void setApellidosAsis(String apellidosAsis) {
        this.apellidosAsis = apellidosAsis;
    }

    public Date getFecNacAsis() {
        return fecNacAsis;
    }

    public void setFecNacAsis(Date fecNacAsis) {
        this.fecNacAsis = fecNacAsis;
    }

    public String getDniAsis() {
        return dniAsis;
    }

    public void setDniAsis(String dniAsis) {
        this.dniAsis = dniAsis;
    }

    public String getSexoAsis() {
        return sexoAsis;
    }

    public void setSexoAsis(String sexoAsis) {
        sexoAsis = sexoAsis;
    }

    public String getEmailAsis() {
        return emailAsis;
    }

    public void setEmailAsis(String emailAsis) {
        this.emailAsis = emailAsis;
    }

    public String getTelefonoAsis() {
        return telefonoAsis;
    }

    public void setTelefonoAsis(String telefonoAsis) {
        this.telefonoAsis = telefonoAsis;
    }
}


