package com.senati.appseminario.presenter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.senati.appseminario.R;

import java.util.Date;
/*hola*/

public class InicioSeminarioActivity extends AppCompatActivity {
    public static final String TAG="InicioSeminarioActivity";

    EditText edtIdAsis, edtNombresAsis, edtApellidosAsis, edtFecNacAsis, edtDniAsis, edtSexoAsis, edtEmailAsis, edtTelefonoAsis;
    Button btnNuevo,btnActualizar,btnEliminar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio_seminario);
        this.initObjects(); //Inicializando objetos
    }

    protected void initObjects(){
        edtIdAsis=findViewById(R.id.edtIdAsis);
        edtNombresAsis=findViewById(R.id.edtNombresAsis);
        edtApellidosAsis=findViewById(R.id.edtApellidosAsis);
        edtFecNacAsis=findViewById(R.id.edtFecNacAsis);
        edtDniAsis=findViewById(R.id.edtDniAsis);
        edtSexoAsis=findViewById(R.id.edtSexoAsis);
        edtEmailAsis=findViewById(R.id.edtEmailAsis);
        edtTelefonoAsis=findViewById(R.id.edtTelefonoAsis);

        btnNuevo=findViewById(R.id.btnNuevo);
        btnActualizar=findViewById(R.id.btnActualizar);
        btnEliminar=findViewById(R.id.btnEliminar);
    }

    public void onClickNuevo(View view){
        Log.d(TAG,">>>Ingreso a metodo onClickNuevo()");
        Intent intent = new Intent(this,RegistroSeminarioActivity.class);
        startActivity(intent);

    }

    public void onClickActualizar(View view){
        Log.d(TAG,">>>Ingreso a metodo onClickActualizar()");
    }

    public void onClickEliminar(View view){
        Log.d(TAG,">>>Ingreso a metodo onClickEliminar()");
    }
}
