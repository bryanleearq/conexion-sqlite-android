package com.arquinigo.loginfirebasesenati;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppCompatActivity {
    public static final String TAG = "LoginActivity";

    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;

    EditText edtLogin, edtClave;
    Button btnIngresar, btnNuevo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initObjects();

        initialize();

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickIngresar(edtLogin.getText().toString(),edtClave.getText().toString());
                Intent intent=new Intent(getApplicationContext(),BienvenidaActivity.class);
                startActivity(intent);
            }
        });

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickNuevoUsuario(edtLogin.getText().toString(),edtClave.getText().toString());
            }
        });

    }

    protected void initObjects(){
        edtLogin = findViewById((R.id.edtLogin));
        edtClave = findViewById((R.id.edtClave));
        btnIngresar = findViewById((R.id.btnIngresar));
        btnNuevo = findViewById((R.id.btnNuevoUsuario));
    }

    private void initialize(){
        Log.d(TAG, "Metodo initialize()");

        firebaseAuth = FirebaseAuth.getInstance();
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser firebaseUser=firebaseAuth.getCurrentUser();

                if(firebaseUser!=null){
                    Log.d(TAG, ">>>onAuthStateChanged - Sign In()");
                    Log.d(TAG, ">>>User ID - "+firebaseUser.getUid());
                    Log.d(TAG, ">>>Email - "+firebaseUser.getEmail());
                }else{
                    Log.d(TAG, "onAuthStateChanged - Sign Out()");
                }
            }
        };
    }

    private void onClickIngresar(String email, String clave){
        firebaseAuth.signInWithEmailAndPassword(email, clave).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Toast.makeText(LoginActivity.this, "Usuario Logueado", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(LoginActivity.this, "Fallo el inicio de sesión", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void onClickNuevoUsuario(String email, String clave){
        firebaseAuth.createUserWithEmailAndPassword(email, clave).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Toast.makeText(LoginActivity.this, "Usuario Creado", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(LoginActivity.this, "Fallo la creación de Usuario", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(authStateListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        firebaseAuth.removeAuthStateListener(authStateListener);
    }
}
